import React, { Component } from 'react';
import {Route, Switch, BrowserRouter} from 'react-router-dom'
import './App.css';
import Welcome from "./Components/welcome/Welcome";
import Contact from "./Components/contacts/Contacts"
import Clock from "./Components/clock/Clock"
import Navigation from "./Components/navigation/Navigation"
import FourOFour from "./Components/fourofour/FourOFour"

class App extends Component {
  render() {
    return (
      <BrowserRouter basename={process.env.PUBLIC_URL}>
        <div className="App">
          <Navigation />
          <Switch>
            <Route path="/welcome/:name" component={Welcome} />
            <Route
              exact
              path="/"
              render={(props) => <Welcome {...props} name= "Greg" />}
            />
            
            <Route exact path="/clock" component={Clock} />
            <Route exact path="/contacts" component={Contact} />
            <Route path="*" component={FourOFour} />
          </Switch>
        </div>
      </BrowserRouter>
    );
  }
}
export default App;




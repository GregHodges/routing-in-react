import React from 'react'
import { Link } from "react-router-dom";


class FourOFour extends React.Component {
    render(){
        return (
          <div className="FourOFour">
            <h1 className= "FourOFour">
              404 Error!
            </h1>
              <p>There is no way we are letting you view this.</p>
            <Link to="/">HomePage</Link>
          </div>
        );
    }
    
}

export default FourOFour